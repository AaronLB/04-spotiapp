import { Component } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { SpotifyService } from 'src/app/services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent {

  artist: any = {};
  tracks: any[] = [];
  loading:boolean;

  constructor(
    private router: ActivatedRoute,
    private spotify:SpotifyService
  ){
    this.loading = true;
    this.router.params.subscribe( params => {
      this.getArtist(params['id']);
      this.getTopTracks(params['id']);
    });
  }

  getArtist( id:string ){
    this.spotify.getArtist(id)
                .subscribe( artist => {
                  // console.log(artist);
                  this.loading=false;
                  this.artist = artist;
                });
  }

  getTopTracks(id:string){
    this.spotify.getArtistTopTracks(id)
                .subscribe( tracks =>{
                  console.log(tracks);
                  this.tracks =tracks;
                })
  }

}
