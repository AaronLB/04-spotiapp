import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  
  newTranks: any[] = [];
  loading: boolean;

  error: boolean = false;
  messageError: string = '';

  constructor( private spotify:SpotifyService ){
    this.loading = true;
    this.spotify.getNewReleases()
                .subscribe( data => {
                  console.log(data);
                  this.newTranks = data;
                  this.loading = false;
                },
                ( errorServices ) =>{
                  this.loading = false;
                  this.error = true;
                  this.messageError=errorServices.error.error.message;
                  console.log(errorServices);
                  console.log(errorServices.error.error.message);
                  
                });
  }
}
