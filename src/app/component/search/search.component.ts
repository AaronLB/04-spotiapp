import { Component } from '@angular/core';
import { SpotifyService } from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent {

  artists: any[] = [];
  albums: any[] = [];
  loading:boolean;

  constructor(
    private spotify:SpotifyService
  ){
    this.loading=false;
  }

  search(data:string){
    console.log(data);
    this.loading = true;
    this.spotify.getSerch(data).subscribe( (data:any) =>{
      this.loading = false;
      this.artists = data.artists;
      this.albums = data.albums;
      
    });
  }

}
