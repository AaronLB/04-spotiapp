import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class SpotifyService {

  constructor( private http:HttpClient ) { 
    // console.log('service Spotify');
    
  }

  getQuery( query: string ){

    const url = `https://api.spotify.com/v1/${ query }`;
    
    const headers = new HttpHeaders({
      'Authorization':'Bearer BQDHuLHUvjYpjYtU_m61ndQsWra42z2JZSK6WTNBJsQXkZuphxpGKOGGfG1PQxTDrS7gG50SiBp-Yku31nDMRujMOLujUiG_yd8PXRdV3mjfaQrKFh4'
    });
  
    return this.http.get( url, { headers });
  }

  getNewReleases (){

    return this.getQuery('browse/new-releases')
                .pipe( map( ( data:any ) => data.albums.items ));

    // return this.http.get('https://api.spotify.com/v1/',{ headers })
                    
              /* .subscribe( ( release ) =>{
                console.log(release);
              }); */
  }

  getSerch( data:string ){
    
    return this.getQuery(`search?q=${ data }&type=album%2Cartist&include_external=audio`)
              .pipe( map( (data:any)=> {
                  return{'artists' : data.artists.items,'albums': data.albums.items} 
              }));
    //  this.http.get(`https://api.spotify.com/v1/search?q=${ data }&type=album%2Cartist&include_external=audio`,{ headers })
  }

  getArtist(id:string){
    return this.getQuery(`artists/${ id }`)
              /* .pipe( map( (data:any)=> {
                  console.log(data);
                  
              })) */;
  }
  getArtistTopTracks(id:string){
    return this.getQuery(`artists/${ id }/top-tracks?market=mx`)
              .pipe( map( (data:any)=> {
                return data.tracks 
                  
              }));
  }
}
