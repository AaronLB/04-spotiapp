import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from "@angular/router";

import { HttpClientModule } from "@angular/common/http";

import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { SearchComponent } from './component/search/search.component';
import { ArtistComponent } from './component/artist/artist.component';
import { NavbarComponent } from './component/shared/navbar/navbar.component';

// * Importar rutas

import { ROUTES } from './app.routes';

// * Pipes

import { NoimagePipe } from './pipes/noimage.pipe';
import { CardsComponent } from './component/shared/cards/cards.component';
import { LoadingComponent } from './component/shared/loading/loading.component';
import { DomeseguroPipe } from './pipes/domeseguro.pipe';


// * rutas
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    SearchComponent,
    ArtistComponent,
    NavbarComponent,
    NoimagePipe,
    CardsComponent,
    LoadingComponent,
    DomeseguroPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot( ROUTES, { useHash: true } )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
