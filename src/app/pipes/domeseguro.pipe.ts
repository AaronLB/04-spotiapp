import { HttpClient } from '@angular/common/http';
import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from "@angular/platform-browser";
import { map } from "rxjs/operators";

@Pipe({
  name: 'domeseguro'
})
export class DomeseguroPipe implements PipeTransform {

  constructor( private http:HttpClient ,private domSanitizer:DomSanitizer ){

  }

  transform(value: string): any {
    const url = 'https://open.spotify.com/embed/track/';
    const track = this.domSanitizer.bypassSecurityTrustResourceUrl( url + value+"?utm_source=oembed" );
    // console.log(track);
    return track;
  }

}
