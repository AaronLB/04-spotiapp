import { Routes } from "@angular/router";
import { HomeComponent } from "./component/home/home.component";
import { SearchComponent } from "./component/search/search.component";
import { ArtistComponent } from "./component/artist/artist.component";

export const ROUTES: Routes = [
    { path: 'home', component: HomeComponent },
    { path: 'search', component: SearchComponent },
    { path: 'artist/:id', component: ArtistComponent },
    /* 
    ? comodines para que cualquier ruta que no este definida redirija a home
    Todo path: '' -> es para cualquier ruta
    */
    { path: '', pathMatch:'full', redirectTo:'home' },
    { path: '**', pathMatch:'full', redirectTo:'home' }
];